#!/usr/bin/env bash
# Checkout previous git commit
# Usage: gp

git checkout HEAD^ 2> /dev/null

echo "Current revision: "`git log -1 --format=oneline`
